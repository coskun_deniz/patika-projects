
def flatten(inputs, result=None):

	if result is None:
		result = []

	if not isinstance(inputs, list):
		result.append(inputs)
	else:
		for item in inputs:
			flatten(item, result)

	return result


def main():

	test_input = [[1, 'a', ['cat'], 2], [[[3]], 'dog'], 4, 5]

	print(flatten(test_input))


if __name__ == "__main__":

	main()