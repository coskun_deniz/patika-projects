
def reverse_list(inputs):

	result = []

	for item in reversed(inputs):
		if isinstance(item, list):
			result.append(list(reversed(item)))
		else:
			result.append(item)

	return result


def main():

	test_input = [[1, 2], [3, 4], [5, 6, 7]]

	print(reverse_list(test_input))


if __name__ == "__main__":

	main()